# Flutter Docker images

Functional multiarch images for Flutter CI/CD.

## Features

- supported architectures
    - aarch64 / arm64v8
    - x86_64 / amd64
- Clean build from Debian base
- Extensible via APT
- Includes Android SDK
- Easy to use Android AVD for integration tests
- different flavors
    - basic: no platform specific build tools shipped, only for tests
    - web: only web toolchain shipped
    - linux: Linux desktop toolchain shipped
        - debian: Debian build tools shipped
    - android: Android toolchain shipped
        - shipped components:
            - [android/sdk_components](android/sdk_components) - all default SDK components
            - [android/emulator_components](android/emulator_components) - the build tools shipped on x86_64
            - [emulator/emulator_components](emulator/emulator_components) - the emulator system images shipped on
              x86_64

## Available tags

```shell
export FLUTTER_VERSION=3.29.0
# the base image without any specific toolchain, e.g. for tests
docker pull registry.gitlab.com/polycule_client/flutter-dockerimages:$FLUTTER_VERSION-base
# Linux
docker pull registry.gitlab.com/polycule_client/flutter-dockerimages:$FLUTTER_VERSION-linux
# Debian build image
docker pull registry.gitlab.com/polycule_client/flutter-dockerimages:$FLUTTER_VERSION-debian
# web
docker pull registry.gitlab.com/polycule_client/flutter-dockerimages:$FLUTTER_VERSION-web
# Android
docker pull registry.gitlab.com/polycule_client/flutter-dockerimages:$FLUTTER_VERSION-android
```

All images have annotated manifests and will hence serve the right layers for `arm64v8` and `amd64` each.

## Using the android emulator

```shell
export EMULATOR_ANDROID_VERSION=35
export ANDROID_DEVICE=pixel_7_pro
# the auth token for emulator console via telnet
export EMULATOR_AUTH_TOKEN="SomeSecret"
# check the following link for further configuration via environment variables
# https://developer.android.com/tools/variables
docker pull registry.gitlab.com/polycule_client/flutter-dockerimages:emulator-$EMULATOR_ANDROID_VERSION
docker run -d --name $ANDROID_DEVICE --rm --device /dev/kvm \
           -e ANDROID_DEVICE=$ANDROID_DEVICE -e EMULATOR_AUTH_TOKEN=$EMULATOR_AUTH_TOKEN \
           -p 5554:5554/tcp -p 5555:5555/tcp \
           registry.gitlab.com/polycule_client/flutter-dockerimages:emulator-$EMULATOR_ANDROID_VERSION
# wait for the device to boot, check
docker logs $ANDROID_DEVICE

# connect to the emulator via TCP
adb connect localhost:5555

# get graphical access to the emulator
scrcpy

# use adb as usual
adb shell

# use the emulator console
# https://developer.android.com/studio/run/emulator-console
telnet localhost 5554
# $ auth SomeSecret

# stop and remove the emulator
docker stop $ANDROID_DEVICE
```

Using within GitLab CI :

Please ensure your runner meets the following requirements :

 - allow privileged execution
 - expose `/dev/kvm` to the docker builds

```yaml
variables:
  - FLUTTER_VERSION: 3.29.0
  - EMULATOR_ANDROID_VERSION: 35
  - ANDROID_DEVICE: pixel_7_pro
  - EMULATOR_AUTH_TOKEN: shared-secret

my_integration_test:
  services:
    - name: registry.gitlab.com/polycule_client/flutter-dockerimages:emulator-$EMULATOR_ANDROID_VERSION
      alias: emulator
  image: registry.gitlab.com/polycule_client/flutter-dockerimages:$FLUTTER_VERSION-android
  before_script:
    - adb start-server
    - adb connect emulator:5555
  script:
    # build your application
    - ...
    # start recording
    - scrcpy --no-audio --no-window -r recording.mkv &
    # run your tests
    - ...
  after_script:
    - pkill scrcpy || true
    # convert matroska to mp4
    - ffmpeg -i recording.mkv -c copy recording.mp4 || true
  artifacts:
    when: always
    paths:
      - recording.mp4
```

### Included packages

The images contain all required and optional dependencies for running Flutter commands. Both build and testing
dependencies are provided. In particular, this is :

- Flutter base :
    - `curl`
    - `bash`
    - `file`
    - `git`
    - `unzip`
    - `xz-utils`
    - `zip`
- Flutter test :
    - `libglu1-mesa`
- Additional basic system utilities
    - `wget`
    - `yq`
    - `jq`
    - `lcov`
- Web dependencies :
    - `chromium-driver`, `chromium`
    - `geckodriver`, `firefox`
- Linux dependencies :
    - `clang`
    - `cmake`
    - `git`
    - `ninja-build`
    - `pkg-config`
    - `libgtk-3-dev`
    - `liblzma-dev`
    - `libstdc++-14-dev`
- Debian dependencies
    - `build-essential`
    - `dh-make`
- Android dependencies :
    - `openjdk-21-jdk`
    - `libstdc++-14-dev`
    - `libpulse0`
    - `ffmpeg`
    - `scrcpy`
    - `bundletool`
    - [Android SDK](#features)

#### Shipped plugin dependencies

| Plugin                                                                              | Linux                                                              | Android                        |
|-------------------------------------------------------------------------------------|--------------------------------------------------------------------|--------------------------------|
| [`package:flutter_rust_bridge`](https://pub.dev/packages/flutter_rust_bridge)       | [`rustup`](https://rustup.rs/)                                     | [`rustup`](https://rustup.rs/) |
| [`package:flutter_secure_storage`](https://pub.dev/packages/flutter_secure_storage) | [`libjsoncpp-dev`](https://github.com/open-source-parsers/jsoncpp) |                                |
| [`package:flutter_secure_storage`](https://pub.dev/packages/flutter_secure_storage) | [`libsecret-1-dev`](https://gitlab.gnome.org/GNOME/libsecret)      |                                |
| [`package:media_kit`](https://pub.dev/packages/media_kit)                           | [`libmimalloc-dev`](https://github.com/microsoft/mimalloc)         |                                |
| [`package:media_kit`](https://pub.dev/packages/media_kit)                           | [`libmpv-dev`](https://mpv.io/)                                    |                                |
| [`package:sqflite_common_ffi`](https://pub.dev/packages/sqflite_common_ffi)         | [`libsqlite3-dev`](https://www.sqlite.org/)                        |                                |
| [`package:sqlcipher_flutter_libs`](https://pub.dev/packages/sqlcipher_flutter_libs) | [`libssl-dev`](https://openssl-library.org/)                       |                                |

Missing a common dependency ? Feel free to open an issue !

### Channel images

*Do you support channel images, e.g. `stable` ?*

No. Every build should always pin to a particular Flutter version - as much as to particular dependency versions.
You will otherwise soon mess up your project. If you locally want to use a particular branch, you can build the images
on your own.

### Building the images locally

```shell
export FLUTTER_VERSION=3.29.0
export EMULATOR_ANDROID_VERSION=35

# build the base image for a specified Flutter version
docker buildx build --build-arg FLUTTER_VERSION=$FLUTTER_VERSION -t registry.gitlab.com/polycule_client/flutter-dockerimages:$FLUTTER_VERSION-base base
# build extended images (possible values : web, android, linux, debian)
docker buildx build --build-arg FLUTTER_VERSION=$FLUTTER_VERSION -t registry.gitlab.com/polycule_client/flutter-dockerimages:$FLUTTER_VERSION-android android

# build the emulator image
docker buildx build --build-arg EMULATOR_ANDROID_VERSION=$EMULATOR_ANDROID_VERSION -t registry.gitlab.com/polycule_client/flutter-dockerimages:emulator-$EMULATOR_ANDROID_VERSION emulator

# build the base image for a specified Flutter channel (stable, beta, dev, master)
docker buildx build --build-arg FLUTTER_VERSION=beta --build-arg FLUTTER_CHANNEL=beta -t registry.gitlab.com/polycule_client/flutter-dockerimages:beta-base base
```

## License

This packaged according to the terms and conditions of EUPL-1.2.
