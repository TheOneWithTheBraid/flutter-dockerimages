#!/usr/bin/env bash
set -em # enable job control and stop on script failures

# if the user specifies an auth_token, enable telnet
if [ -n "${EMULATOR_AUTH_TOKEN}" ]; then
  echo "${EMULATOR_AUTH_TOKEN}" > ~/.emulator_console_auth_token
fi

# create the emulator config
avdmanager create avd --force --package "system-images;android-${EMULATOR_ANDROID_VERSION};default;${EMULATOR_ARCH}" --device "${ANDROID_DEVICE}" --name "${ANDROID_DEVICE}"

# fix system-images path
# scale down emulator screen to 50 %
sed -i -E '
s/Sdk\/system-images/system-images/g
s/hw.lcd.height=.+/hw.lcd.height=1520/g
s/hw.lcd.width=.+/hw.lcd.width=720/g
s/hw.lcd.density=.+/hw.lcd.density=280/g
' "${ANDROID_AVD_HOME}/${ANDROID_DEVICE}.avd/config.ini"

# start emulator
emulator -avd "${ANDROID_DEVICE}" \
          -ports "${SERIAL}","${ADB}" \
          -cores "$(nproc)" -gpu host -no-audio \
          -writable-system -wipe-data \
          -no-boot-anim -no-metrics -no-window \
          &

echo "Waiting for emulator to be available..."
while ( ! adb devices | grep "${ANDROID_SERIAL}" ) || ( adb devices | grep offline ); do
  sleep 5
done
echo "Emulator is connected."

# disable bluetooth, does not work for Android 13 emulator
#adb shell cmd bluetooth_manager disable

# make telnet and ADB reachable from outside the container
# socat required since the emulator only allows localhost
socat tcp6-listen:5554,bind=::1,fork tcp4:127.0.0.1:"${SERIAL}" &
socat tcp6-listen:5555,bind=::1,fork tcp4:127.0.0.1:"${ADB}" &
# and once again for IPv4
socat tcp4-listen:5554,bind=0.0.0.0,fork tcp4:127.0.0.1:"${SERIAL}" &
socat tcp4-listen:5555,bind=0.0.0.0,fork tcp4:127.0.0.1:"${ADB}" &
fg %1 # Switch main process to emulator
